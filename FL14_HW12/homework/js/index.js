function visitLink(path) {
	if (localStorage.length == 0) {
		localStorage.setItem('1', '0');
		localStorage.setItem('2', '0');
		localStorage.setItem('3', '0');
	}
	if (path === 'Page1') {
		let counter1 = Number(localStorage.getItem('1')) + 1;
		localStorage.setItem('1', counter1);
	} else if (path === 'Page2') {
		let counter2 = Number(localStorage.getItem('2')) + 1;
		localStorage.setItem('2', counter2);
	} else if (path === 'Page3') {
		let counter3 = Number(localStorage.getItem('3')) + 1;
		localStorage.setItem('3', counter3);
	}
}

function viewResults() {
	const container = document.querySelector('.container');
	let resultList = document.createElement('ul');
	container.appendChild(resultList);
	for (let i = 1; i <= localStorage.length; i++) {
		let results = document.createElement('li');
		resultList.appendChild(results);
		if (localStorage[i] === undefined) {
			localStorage.setItem(i, '0');
		}
		results.innerHTML = 'You visited Page' + i + ' ' + localStorage.getItem(i) + ' time(s)';
	}
	if (localStorage.length === 0) {
		let results = document.createElement('li');
		resultList.appendChild(results);
		results.innerHTML = 'No pages were visited.';
	}
	return localStorage.clear();
}