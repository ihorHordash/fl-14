let input = prompt('Enter your mathematical expression ');
calculator(input);

function calculator(exp) {
    const expression = exp.split('').join('');
    let result;
    try {
        result = eval(expression);
        if (result === undefined || isNaN(result) === true) {
            throw new Error('Invalid input');
        }
        if (result === Infinity || result === -Infinity) {
            throw new Error('Result is infinity');
        }
        alert(`${expression} = ${result}`);
        getNewExpr();

    } catch (err) {
        if (err instanceof SyntaxError) {
            alert(new Error(err.message + '. Please enter another expression:'));
        } else if (err instanceof ReferenceError) {
            alert(new Error(err.message + '. Please enter another expression:'));
        } else {
            alert(new Error(err.message + '. Please enter another expression:'));
        }
        getNewExpr();
    }
}

function getNewExpr() {
    let input2 = prompt('Enter your mathematical expression ');
    calculator(input2);
}