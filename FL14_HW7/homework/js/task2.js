const inputWord = prompt('Input your word');
const divider = 2;
let charArray = inputWord.split('');
if (inputWord.trim().length === 0) {
    alert('Invalid value');
} else if (charArray.length % divider !== 0) {
    let oMiddle = Math.floor(charArray.length / divider);
    alert(charArray[oMiddle]);
} else {
    let eMiddle = charArray.length / divider;
    if (charArray[eMiddle - 1] === charArray[eMiddle]) {
        alert('Middle characters are the same');
    } else {
        alert(charArray[eMiddle - 1] + charArray[eMiddle]);
    }
}