const batterys = Number(prompt('How many betteries you have?', ''));
const rate = Number(prompt('What is the percentage of defective batteries?', ''));
const maxRate = 100;
const afterComma = 2;

if (isNaN(batterys) | isNaN(rate)) {
    alert('Invalid input data');
} else if (batterys < 0 | (rate < 0 | rate > maxRate)) {
    alert('Invalid input data');
} else {
    let defective = batterys * (rate / maxRate);
    let working = batterys - defective;
    if (Number.isInteger(defective) === false) {
        let defectiveFloat = defective.toFixed(afterComma);
        let workingFloat = working.toFixed(afterComma);
        alert(`Amount of batteries: ${batterys}
Defective rate: ${rate}%
Amount of defective batteries: ${defectiveFloat}
Amount of working batteries: ${workingFloat}`);
    } else {
        alert(`Amount of batteries: ${batterys}
Defective rate: ${rate}%
Amount of defective batteries: ${defective}
Amount of working batteries: ${working}`);
    }
}