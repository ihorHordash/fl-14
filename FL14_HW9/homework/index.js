//task 1
function convert(...inputArray) {
    let convertedArray = [];
    for (let i = 0; i < inputArray.length; i++) {
        if (typeof inputArray[i] === typeof 'string') {
            convertedArray.push(parseInt(inputArray[i]));
        } else {
            convertedArray.push(inputArray[i].toString());
        }
    }
    console.log(convertedArray);
}


//task 2
function executeforEach(inputArray, fn) {
    for (let i = 0; i < inputArray.length; i++) {
        inputArray[i] = fn(inputArray[i]);
    }
}


//task 3
function mapArray(inputArray, fn) {
    let arrMapped = [];
    executeforEach(inputArray, function (el) {
        if(typeof el !== Number){
            el = parseInt(el);
        }
        arrMapped.push(fn(el));
    });
    return arrMapped;
}


//task 4
function filterArray(inputArray, fn) {
    let clearArray = [];
    executeforEach(inputArray, (el) => {
        if (fn(el) === true) {
            clearArray.push(el);
        }
    })
    return clearArray;
}

//task 5
function getValuePosition(arr, value) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === value) {
            return i + 1;
        }
    }
    return false;
}

//task 6
function flipOver(str) {
    let flipedString = '';
    for (let i = 0; i < str.length; i++) {
        flipedString = str[i] + flipedString;
    }
    return flipedString;
}

//task 7
function makeListFromRange(inputArray) {
    let list = [];
    if (inputArray[0] === inputArray[1]) {
        return inputArray;
    } else if (inputArray[0] < inputArray[1]) {
        while (inputArray[0] <= inputArray[1]) {
            list.push(inputArray[0]);
            inputArray[0] = inputArray[0] + 1;
        }
        return list;
    } else {
        while (inputArray[0] >= inputArray[1]) {
            list.push(inputArray[0]);
            inputArray[0] = inputArray[0] - 1;
        }
        return list;
    }
}


//task 8
function getArrayOfKeys(arr, key) {
    let valueByKey = [];
    executeforEach(arr, function (el) {
        return valueByKey.push(el[key]);
    });
    return valueByKey;
}
const fruits = [{
        name: 'apple',
        weight: 0.5
    },
    {
        name: 'pineapple',
        weight: 2
    }
];

//task 9
function getTotalWeight(arr) {
    let totalWeight = 0;
    executeforEach(arr, function (el) {
        totalWeight += el['weight'];
        return totalWeight;
    })
    return totalWeight
}

const basket = [{
        name: 'Bread',
        weight: 0.3
    },
    {
        name: 'Coca - Cola',
        weight: 0.5
    },
    {
        name: 'Watermelon',
        weight: 8
    }
];

//task 10
function getPastDay(arr, past) {
    let newDate = new Date(arr);
    newDate.setDate(arr.getDate() - past);
    let dateNumber = newDate.getDate();
    let dateFromat = {
        day: 'numeric',
        month: 'short',
        year: 'numeric'
    };
    return dateNumber + ',  (' + newDate.toLocaleDateString('en-GB', dateFromat) + ')';
}
const yearTask10 = 2020;
const dayTask10 = 2;
const date = new Date(yearTask10, 0, dayTask10);

//task 11
function formatDate(date) {
    let month, twelwMonth, day, hour, min;
    const towDigitNum = 10;
    const checker = 9;
    if (date.getMonth() < checker) {
        twelwMonth = date.getMonth() + 1;
        month = '0' + twelwMonth;
    } else {
        month = date.getMonth() + 1;
    }
    if (date.getDate() < towDigitNum) {
        day = '0' + date.getDate();
    } else {
        day = date.getDate();
    }
    if (date.getHours() < towDigitNum) {
        hour = '0' + date.getHours();
    } else {
        hour = date.getHours();
    }
    if (date.getMinutes() < towDigitNum) {
        min = '0' + date.getMinutes();
    } else {
        min = date.getMinutes();
    }
    return date.getFullYear() + '/' + month + '/' + day + '/ ' + hour + ':' + min;
}