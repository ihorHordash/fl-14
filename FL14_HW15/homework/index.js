/* START TASK 1: Your code goes here */

const table = document.querySelector('table');
table.addEventListener('click', function (e) {
    if (e.target.className === 'cell') {
        if (e.target === e.target.parentNode.firstElementChild) {
            e.target.parentNode.classList.add('blue');
        } else {
            e.target.classList.add('yellow');
        }
        if (e.target.hasAttribute('id', 'special-cell')) {
            e.target.parentNode.parentNode.classList.add('green');
        }
    }
})
/* END TASK 1 */

/* START TASK 2: Your code goes here */
const message = document.querySelector('.message');
const inputField = document.forms[0].firstElementChild;
const sendBtn = document.forms[0].lastElementChild;
inputField.onfocus = function () {
    inputField.setAttribute('value', '')
    inputField.addEventListener('keyup', function () {
        if (inputField.value.match(/^\+38?(0\d{9})$/)) {
            sendBtn.disabled = false
            message.innerHTML = 'Type format follows format  +380*********';
            message.classList.add('valid');
        } else {
            console.log('wrong input')
            sendBtn.disabled = true;
            message.innerHTML = 'Type format does not follow format  +380*********';
            message.classList.remove('valid')
            message.classList.add('invalid');
            return false
        }
    })


}
sendBtn.addEventListener('click', function (e) {
    e.preventDefault();
    message.innerHTML = 'Data was succesfuly sent'
})

/* END TASK 2 */

/* START TASK 3: Your code goes here */

const court = document.querySelector('#court');
const ball = document.querySelector('.ball');
let scoreA = document.querySelector('.scoreA');
let scoreB = document.querySelector('.scoreB');
const basketA = document.querySelector('.basketA');
const basketB = document.querySelector('.basketB');
const goal = document.querySelector('.goal');
const two = 2;
const timeonut = 2000;

court.onclick = function (event) {
    let fieldCoords = this.getBoundingClientRect();
    let ballCoords = {
        top: event.clientY - fieldCoords.top - court.clientTop - ball.clientHeight / two,
        left: event.clientX - fieldCoords.left - court.clientLeft - ball.clientWidth / two
    }

    ballCoords.top < 0 ? ballCoords.top = 0 : '';
    ballCoords.left < 0 ? ballCoords.left = 0 : '';

    if (ballCoords.left + ball.clientWidth > court.clientWidth) {
        ballCoords.left = court.clientWidth - ball.clientWidth;
    }
    if (ballCoords.top + ball.clientHeight > court.clientHeight) {
        ballCoords.top = court.clientHeight - ball.clientHeight;
    }

    ball.style.left = ballCoords.left + 'px';
    ball.style.top = ballCoords.top + 'px';
}

court.addEventListener('click', (e) => {
    if (e.target === basketA) {
        console.log('team B score');
        goal.innerHTML = 'Team B score goal'
        scoreB.innerHTML = Number(scoreB.innerHTML) + 1;
        setTimeout(function () {
            goal.innerHTML = ''
            ball.style.left = '-5px';
            ball.style.top = '145px';
        }, timeonut);

    } else if (e.target === basketB) {
        console.log('Team A score goal');
        goal.innerHTML = 'Team A score goal'
        scoreA.innerHTML = Number(scoreA.innerHTML) + 1;
        setTimeout(function () {
            goal.innerHTML = ''
            ball.style.left = '560px';
            ball.style.top = '145px';
        }, timeonut);
    }
})

/* END TASK 3 */