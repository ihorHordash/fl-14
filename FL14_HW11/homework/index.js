//task 1    
const isEquals = (a, b) => a === b;

//task 2
const numberToString = (number) => number.toString();

//task 3
const storeNames = (...names) => names;

// task 4
const getDivision = (a, b) => a > b ? a / b : b / a;

//task 5
let negativeCount = (arr) => {
    let counter = 0;
    arr.forEach((el) => {
        el < 0 ? counter++ : counter;
    })
    return counter;
}

//task 6
let letterCount = (word, letter) => {
    let arr = word.split('');
    let counter = 0;
    arr.forEach((el) => {
        el === letter ? counter++ : counter
    });
    return counter;
}

//task 7
let countPoints = (championship) => {
    const win = 3;
    const draw = 1;
    let counter = 0;
    championship.forEach((el) => {
        el = el.split(':')
        if (Number(el[0]) > Number(el[1])) {
            counter = counter + win;
        } else if (Number(el[0]) === Number(el[1])) {
            counter = counter + draw;
        }
    })
    return counter
}