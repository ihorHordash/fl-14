const data = [{
    'folder': true,
    'title': 'Grow',
    'children': [{
        'title': 'logo.png'
      },
      {
        'folder': true,
        'title': 'English',
        'children': [{
          'title': 'Present_Perfect.txt'
        }]
      }
    ]
  },
  {
    'folder': true,
    'title': 'Soft',
    'children': [{
        'folder': true,
        'title': 'NVIDIA',
        'children': null
      },
      {
        'title': 'nvm-setup.exe'
      },
      {
        'title': 'node.exe'
      }
    ]
  },
  {
    'folder': true,
    'title': 'Doc',
    'children': [{
      'title': 'project_info.txt'
    }]
  },
  {
    'title': 'credentials.txt'
  }
];

const rootNode = document.getElementById('root');

let treeBox = document.createElement('div');
rootNode.appendChild(treeBox);
treeBox.classList.add('tree-box')

const createTree = arr => {
  let html = `<ul>`;
  for (let {
      folder,
      title,
      children
    } of arr) {
    children = Array.isArray(children) ? createTree(children) : '';
    folder = folder ? 'folder' : 'insert_drive_file';
    html += `<li class="right-click-area"> <span class="material-icons orange600" >${folder}</span>${title}</li>`;
    html += children;
  }
  html += `</ul>`;
  return html
}

treeBox.insertAdjacentHTML('beforeend', createTree(data));

const createMenu = () => {
  let html = `<ul class="right-click-menu">`;
  html += `<li class="rename">Rename</li>`;
  html += `<li class="tree-clear">Delete</li>`;
  html += `</ul>`;
  return html
}
treeBox.insertAdjacentHTML('beforeend', createMenu());
const menu = document.querySelector('.right-click-menu');

const folderList = document.querySelectorAll('li');
for (let folder of folderList) {
  folder.addEventListener('click', function () {
    let x = this.nextElementSibling;
    if (x.className === 'show') {
      x.classList.remove('show')
      if (this.children[0].innerHTML === 'folder' || this.children[0].innerHTML === 'folder_open') {
        this.children[0].innerHTML = 'folder'
      }

    } else {
      x.classList.add('show');
      if (this.children[0].innerHTML === 'folder' || this.children[0].innerHTML === 'folder_open') {
        this.children[0].innerHTML = 'folder_open'
      }
    }
  })
}

const menuArea = document.querySelectorAll('.right-click-area');
for (let area of menuArea) {
  area.addEventListener('contextmenu', event => {
    event.preventDefault();
    menu.classList.add('active');
    menu.style.top = `${event.clientY}px`;
    menu.style.left = `${event.clientX}px`;
    console.log('correct')
    document.querySelector('.rename').addEventListener('click', () => {
      area.contentEditable = true;
      area.innerHTML.focus();
      area.onblur = () => {
        area.contentEditable = false;
      }
    });
    document.querySelector('.tree-clear').addEventListener('click', () => {
      area.remove();
      area.remove().children
    })
  })
}
const secondButton = 2;
document.addEventListener('click', event => {
  if (event.button !== secondButton) {
    menu.classList.remove('active');
  }
}, false);

menu.addEventListener('click', event => {
  event.stopPropagation();
}, false)

treeBox.addEventListener('contextmenu', event => {
  event.preventDefault();
});