//task 1 
const getAge = (yy, mm, dd) => {
    const birthday = new Date(yy, mm - 1, dd);
    const today = new Date();
    let age = today.getFullYear() - birthday.getFullYear();
    if (birthday.getMonth() === today.getMonth() && birthday.getDate() > today.getDate() ||
        birthday.getMonth() > today.getMonth()) {
        return age - 1;
    } else {
        return age;
    }
}

//task 2 
const getWeekDay = (date) => {
    let day = new Date(date);
    return day.toLocaleString('en-EN', {
        weekday: 'long'
    });
}

//task 3 
let getProgrammersDay = year => {
    const progDay = 256;
    let day = new Date(year, 0, progDay);
    return day.getDate() + ' ' + day.toLocaleString('en-EN', {
        month: 'short'
    }) + ', ' + day.getFullYear() + ' (' + getWeekDay(day) + ')';
}

//task 4
function howFarIs(day) {
    let dayToLowerCase = day.toLowerCase();
    let weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let i;
    const today = new Date();
    weekday.forEach((el, index) => {
        if (dayToLowerCase === el.toLowerCase()) {
            i = index;
        }
    });
    if (i > today.getDay()) {
        return 'It\'s ' + (i - today.getDay()) + 'day(s) left till ' + weekday[i] + '.';
    } else if (i < today.getDay()) {
        return 'It\'s ' + (weekday.length - (today.getDay() - i)) + 'day(s) left till ' + weekday[i] + '.';
    } else {
        return 'Hey, today is ' + weekday[i] + ' =)';
    }
}

//task 5
function isValidIdentifier(input) {
    let regexp = /^[a-zA-Z_$][0-9a-zA-Z_$]*$/
    let result = regexp.test(input);
    return result;
}

//task 6
function capitalize(string) {
    let result = string.replace(/(\w+)/g, function (char) {
        return char[0].toUpperCase() + char.substring(1);
    });
    console.log(result);
}

//task 7 
function isValidAudioFile(input) {
    let regexp = /^[a-z0-9]+\.mp3$|.flac$|.alac$|.aac$/i;
    let result = regexp.test(input);
    console.log(result);
}

//task 8 
function getHexadecimalColors(input) {
    let regexp = /#([a-f0-9]{3}){1,2}\b/gi;
    let result;
    if (regexp.test(input) === true) {
        result = input.match(regexp);
    } else {
        result = [];
    }
    console.log(result);
}

//task 9
function isValidPassword(password) {
    let regexp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g;
    let result = regexp.test(password);
    console.log(result);
}

//task 10
let addThousandsSeparators = (number) => {
    let result = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    console.log(result);
}