const externalList = JSON.parse(localStorage.getItem('myQuestions'));

function shuffledQuestionsList() {
    let currentIndex = externalList.length;
    let temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = externalList[currentIndex];
        externalList[currentIndex] = externalList[randomIndex];
        externalList[randomIndex] = temporaryValue;
    }
}
const questionText = document.getElementById('question-text');
const optionContainer = document.querySelector('.option-container');
const homeBox = document.querySelector('.home-box');
const quizBox = document.querySelector('.quiz-box');
const resultBox = document.querySelector('.result-box');
const milion = 1000000;
const resultNote = document.getElementById('score');
const nextButton = document.getElementById('next');
const skip = document.getElementById('skip-btn');
let questionScore = document.getElementById('question-score'); //prize for stage display
let totalScore = document.querySelector('.total-score'); //total prize dispaly


let questionCounter = 0;
let currentQuestion;
let availableQuestions = [];
let availableOptions = [];
let prizeCounter = 0;
let firstQuestionPrize = 100;
let stagePrize = firstQuestionPrize;
let currentPrize;
const prizeMultiplier = 2;



//push shuffled questions to availableQuestions array
function setAvailableQuestions() {
    shuffledQuestionsList()
    const totalQuestion = externalList.length;
    for (let i = 0; i < totalQuestion; i++) {
        availableQuestions.push(externalList[i])
    }
}

//set question and options
function getNewQuestion() {
    nextButton.disabled = true;
    //get random question 
    const questionIndex = availableQuestions[Math.floor(Math.random() * availableQuestions.length)];
    currentQuestion = questionIndex;
    questionText.innerHTML = currentQuestion.question;
    //get position of asked question 
    const index1 = availableQuestions.indexOf(questionIndex);
    //remove question from the list so it doesnot apear again
    availableQuestions.slice(index1, 1);
    //set options
    const optionLen = currentQuestion.content.length
    for (let i = 0; i < optionLen; i++) {
        availableOptions.push(i)
    }

    optionContainer.innerHTML = '';

    //add options to html
    for (let i = 0; i < optionLen; i++) {
        const option = document.createElement('div');
        option.innerHTML = currentQuestion.content[i];
        option.id = i;
        option.className = 'option';
        optionContainer.appendChild(option);
        option.setAttribute('onclick', 'getResult(this)');
    }
    questionScore.innerHTML = stagePrize;
    questionCounter++
}

//get result of attempt 
function getResult(element) {
    const id = Number(element.id);
    if (id === currentQuestion.correct) {
        element.classList.add('correct');
        prizeCounter = prizeCounter + stagePrize;
        console.log(prizeCounter)
        totalScore.innerHTML = parseInt(prizeCounter);
        stagePrize *= prizeMultiplier;
        nextButton.disabled = false;
        if (prizeCounter > milion) {
            gameOver();
        }
    } else {
        element.classList.add('wrong');
        gameOver();
    }

    unclickableOptions();
}

function unclickableOptions() {
    const optionLen = optionContainer.children.length;
    for (let i = 0; i < optionLen; i++) {
        optionContainer.children[i].classList.add('already-answered');

    }
}


function next() {
    if (questionCounter === externalList.length) {
        console.log('game over')
    } else {
        getNewQuestion();
    }
}

function skipQuestion() {
    getNewQuestion()
    skip.disabled = true;
}

function gameOver() {
    quizBox.classList.add('hide');
    resultBox.classList.remove('hide');
    if (prizeCounter < milion) {
        resultNote.innerHTML = 0;
    } else {
        resultNote.innerHTML = '$' + milion;
    }
}

function resetQuiz() {
    questionCounter = 0;
    prizeCounter = 0;
    stagePrize = firstQuestionPrize;
}

function restartGame() {
    resultBox.classList.add('hide');
    quizBox.classList.remove('hide');
    resetQuiz();
}

function goHome() {
    resultBox.classList.add('hide');
    homeBox.classList.remove('hide')
    resetQuiz()
}

function startQuiz() {
    homeBox.classList.add('hide')
    quizBox.classList.remove('hide') 
    setAvailableQuestions();
    getNewQuestion();
}